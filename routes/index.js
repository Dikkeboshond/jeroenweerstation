var express = require('express');
var router = express.Router();
var request = require('request');

/* GET home page. */
router.get('/', function(req, res, next) {
  request('http://api.openweathermap.org/data/2.5/weather?q=eindhoven&APPID=aa09fdb5fea7c2078f9ffb3b78912821', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.render('index', {
        title: "Jeroens awesome weerstation",
        weerbericht: JSON.parse(body)
      });
    }
  });
});

module.exports = router;
