var express = require('express');
var path = require('path');
var app = express();

// aangeven waar de paginas staan
var index = require('./routes/index');
var users = require('./routes/users');

// een public map maken voor plaatjes, css en javascript op /public
app.use(express.static(path.join(__dirname, 'public')));

// Jade instellen als templating engine zodat variabelen in html gebruikt kunnen worden
app.set('view engine', 'jade');

// een views map openstellen voor de jade templates
app.set('views', path.join(__dirname, 'views'));

// website paths instellen
app.use('/', index);
app.use('/users', users);

module.exports = app;
